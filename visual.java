package kmh;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;

class Person{
    private int move; // between 0 and 2
    private String name;
    private int scores;
    
    public void setMove(int move){
        if(move>=0 && move<=2)this.move = move;
    }
     
    public void setName(String name){
        if(!name.isEmpty()){
            if(name.length()>10){
                name = name.substring(0, 11);
            }
            this.name = name.strip();
        }
    }
    
    public void setScores(int scores){
        if(scores>0)this.scores = scores;
    }
    
    public int getMove(){
        return this.move;
    }
     
    public String getName(){
        return this.name;
    }
    
    public int getScores(){
        return this.scores;
    }
    
    public Person(int move, String name,int scores){
        this.setMove(move);
        this.setName(name);
        this.setScores(scores);
    }
    
    public Person(){
        this(0, "Ivan Kurski", 0);
    }
    
    public Person(Person person){
        this(person.getMove(), person.getName(), person.getScores());
    }
    
    
    public String toString(){
        return(String.format("%s: %d points", getName(), getScores()));
    }
    
}

public class visual extends javax.swing.JFrame implements KeyListener{
    
    
    public static Person player1 = new Person(2, "Rangela", 0);
    public static Person player2 = new Person();
    
 
    public static boolean player1_ready = false;
    public static boolean player2_ready = false;
    
    ImageIcon rock = new ImageIcon("C:\\Users\\aleksandarpd\\Documents\\AleksandarPd_11i\\java\\KMH\\src\\kmh\\rock.png");
    ImageIcon paper = new ImageIcon("C:\\Users\\aleksandarpd\\Documents\\AleksandarPd_11i\\java\\KMH\\src\\kmh\\paper.png");
    ImageIcon scissors = new ImageIcon("C:\\Users\\aleksandarpd\\Documents\\AleksandarPd_11i\\java\\KMH\\src\\kmh\\scissors.png");
   
    public visual() {
        initComponents();    
        player1_text.setText(player1.toString());
        player2_text.setText(player2.toString());
        this.addKeyListener(this);
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        player1_text = new javax.swing.JLabel();
        player2_text = new javax.swing.JLabel();
        winning_text = new javax.swing.JLabel();
        rps_image_player2 = new javax.swing.JLabel();
        rps_image_player1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        player1_text.setText("player1: 0 points");

        player2_text.setText("player2: 0 points");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(player1_text, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(119, 119, 119)
                                .addComponent(player2_text, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(rps_image_player1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(rps_image_player2, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(111, 111, 111)
                        .addComponent(winning_text, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(player2_text, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rps_image_player2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(player1_text, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rps_image_player1, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(winning_text, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
    
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new visual().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel player1_text;
    private javax.swing.JLabel player2_text;
    private javax.swing.JLabel rps_image_player1;
    private javax.swing.JLabel rps_image_player2;
    private javax.swing.JLabel winning_text;
    // End of variables declaration//GEN-END:variables

    @Override
    public void keyTyped(KeyEvent arg0) {
       
    }

    @Override
    public void keyPressed(KeyEvent arg0) {
        
        
        if(arg0.getKeyCode() == KeyEvent.VK_A && !player1_ready){
            System.out.print(arg0.getKeyChar());
            player1.setMove(0);
            player1_ready = true;
            rps_image_player1.setIcon(rock);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_S && !player1_ready){
            System.out.print(arg0.getKeyChar());
            player1.setMove(1);
            player1_ready = true;
            rps_image_player1.setIcon(paper);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_D && !player1_ready){
            System.out.print(arg0.getKeyChar());
            player1.setMove(2);
            player1_ready = true;
            rps_image_player1.setIcon(scissors);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_LEFT && !player2_ready){
            System.out.print(arg0.getKeyChar());
            player2.setMove(0);
            player2_ready = true;
            rps_image_player2.setIcon(rock);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_DOWN && !player2_ready){
            System.out.print(arg0.getKeyChar());
            player2.setMove(1);
            player2_ready = true;
            rps_image_player2.setIcon(paper);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_RIGHT && !player2_ready){
            System.out.print(arg0.getKeyChar());
            player2.setMove(2);
            player2_ready = true;
            rps_image_player2.setIcon(scissors);
        }
        
        
        if(player1_ready && player2_ready){
            System.out.println(compareRPS(player1.getMove(), player2.getMove()));    
            switch(compareRPS(player1.getMove(), player2.getMove())){
                case 1:
                    player1.setScores(player1.getScores() + 1);
                    winning_text.setText(String.format("%s wins", player1.getName()));
                    player1_text.setText(player1.toString());
                    break;
                case 2:
                    player2.setScores(player2.getScores() + 1);
                    winning_text.setText(String.format("%s wins", player2.getName()));
                    player2_text.setText(player2.toString());
                    break;
                case 0:
                    winning_text.setText("draw");
                    break;
           
            }
                            
            player1_ready = false;
            player2_ready = false;
            
            
            // tuk trqbva da spi malko 
            rps_image_player1.setIcon(null);
            rps_image_player2.setIcon(null);
            winning_text.setText(null);
            
        }
               
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
       
    }
    
    int compareRPS(int player1_move, int player2_move){
        switch(player1_move){
            case 0:
                switch(player2_move){
                    case 1:
                        return(2);               
                    case 2:
                        return(1);
                }
                break;
            case 1:
                switch(player2_move){
                    case 0:
                        return(1);
                    case 2:
                        return(2);
                }
                break;
            case 2:
                switch(player2_move){
                    case 0:
                        return(2);
                    case 1:
                        return(1);
                }
                break;
            default:
                return 0;
        }
        return 0;
    }
    
    
}
